import { ImprezifyPage } from './app.po';

describe('imprezify App', () => {
  let page: ImprezifyPage;

  beforeEach(() => {
    page = new ImprezifyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
