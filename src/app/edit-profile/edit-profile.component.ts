import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';

import { UserService } from '../user.service';
import { ProfileService } from '../profile.service';
import { WorkExperienceService } from '../work-experience.service';
import { WorkExperienceComponent } from '../work-experience/work-experience.component';

import * as _ from 'lodash';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
  providers: [WorkExperienceComponent]
})
export class EditProfileComponent implements OnInit {
  activeTab = 'basic';
  user = {};
  profile = {
    overview: null,
    title: null,
    birthday: null,
    address: null,
    profile_photos: []
  };

  month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  months = [];
  years = [];
  current_year = new Date().getFullYear();

  constructor(private _router: Router,
              private _notification: NotificationsService,
              private _userService: UserService,
              private _profileService: ProfileService) { }

  ngOnInit() {
    const self = this;
    self._userService.fetchData().subscribe(
      (res) => {
        if (res) {
          self.user = res;
        }
      },
      (error) => { self._notification.error('Request Failed', error); }
    );

    self._profileService.fetchData().subscribe(
      (res) => {
        if (res) {
          self.profile = res;
        }
      },
      (error) => { self._notification.error('Request Failed', error); }
    );

    _.each(new Array(12), (value, key) => {
      self.months.push({ name: self.month_names[key], value: key + 1 });
    });
    _.each(self.range(self.current_year - 30, self.current_year), (value, key) => {
      this.years.push({ name: value, value: value });
    });
  }

  saveProfile() {
    const self = this;
    self._profileService.saveData(self.profile).subscribe(
      (res) => {
        self.profile = res;
        self.activeTab = 'work';
      },
      (error) => { self._notification.error('Request Failed', error); }
    );
  }

  getProfileAddress($event) {
    this.profile.address = $event.formatted_address;
  }

  private range(j, k) {
    return Array
      .apply(null, Array((k - j) + 1))
      .map(function(discard, n){ return n + j; });
  }

}
