import { Injectable } from '@angular/core';
import { Angular2TokenService } from 'angular2-token';

@Injectable()
export class UserService {

  constructor(private http: Angular2TokenService) {
  }

  fetchData(handle = '') {
    return this.http.get('users?handle=' + handle).map(res => res.json());
  }

  saveData(data) {
    return this.http.put('users/1', {user: data}).map(res => res.json());
  }

}
