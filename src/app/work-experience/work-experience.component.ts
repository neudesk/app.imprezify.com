import { Component, OnInit, Input } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { WorkExperienceService } from '../work-experience.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: ['./work-experience.component.css']
})
export class WorkExperienceComponent implements OnInit {

  @Input() months: any[] = [];
  @Input() years: any[] = [];
  @Input() month_names: any[] = [];

  work = {
    id: null,
    title: null,
    company: null,
    location: null,
    from_month: null,
    from_year: null,
    to_month: null,
    to_year: null,
    description: null,
    current: false
  };
  works = [];

  constructor(private _notification: NotificationsService,
              private _workExperienceService: WorkExperienceService) {
  }

  ngOnInit() {
    const self = this;
    self.fetchWorkExperiences();
  }

  fetchWorkExperiences() {
    const self = this;
    self._workExperienceService.fetchData().subscribe(
      (res) => {
        if (res) {
          self.works = res;
        }
      },
      (error) => { self._notification.error('Request Failed', error); }
    );
  }

  saveWorkExperience() {
    const self = this;
    self._workExperienceService.saveData(self.work).subscribe(
      (res) => {
        self.fetchWorkExperiences();
        _.each(self.work, (value, key) => {
          self.work[key] = null;
        });
        self._notification.success('Saved', 'Work experience has been saved.');
      },
      (error) => { self._notification.error('Request Failed', error); }
    );
  }

  deleteWorkExperience(id) {
    const self = this;
    self._workExperienceService.deleteData(id).subscribe(
      (res) => {
        self.fetchWorkExperiences();
        self._notification.success('Deleted', 'Work experience successfully deleted.');
      },
      (error) => { self._notification.error('Request Failed', error); }
    );
  }

  getWorkAddress($event) {
    this.work.location = $event.formatted_address;
  }

  setCurrentWork(work) {
    const self = this;
    self.work = work;
    self.scrollElement('workExperienceForm');
  }

  public scrollElement(id) {
  }

}
