import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Angular2TokenService, A2tUiModule } from 'angular2-token';

import { AppComponent } from './app.component';

import { routes } from './app.router';

import {APP_BASE_HREF} from '@angular/common';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MdInputModule, MdCheckboxModule, MdTabsModule, MdDatepickerModule, MdNativeDateModule, MdSelectModule} from '@angular/material';

import 'hammerjs';

import { Ng2FileDropModule } from 'ng2-file-drop';

import { ProfileService } from './profile.service';
import { UserService } from './user.service';
import { WorkExperienceService } from './work-experience.service';

import {GooglePlaceModule} from 'ng2-google-place-autocomplete';
import { HomepageComponent } from './homepage/homepage.component';
import {ScrollToModule} from 'ng2-scroll-to';

import { SimpleNotificationsModule } from 'angular2-notifications';
import { PublicProfileComponent } from './public-profile/public-profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { WorkExperienceComponent } from './work-experience/work-experience.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    PublicProfileComponent,
    EditProfileComponent,
    WorkExperienceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdInputModule,
    MdTabsModule,
    MdCheckboxModule,
    MdDatepickerModule,
    MdNativeDateModule,
    MdSelectModule,
    routes,
    A2tUiModule,
    Ng2FileDropModule,
    GooglePlaceModule,
    SimpleNotificationsModule.forRoot(),
    ScrollToModule.forRoot()
  ],
  providers: [Angular2TokenService,
              {provide: APP_BASE_HREF, useValue : '/' },
              ProfileService,
              UserService,
              WorkExperienceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
