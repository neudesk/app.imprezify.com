import { Injectable } from '@angular/core';
import { Angular2TokenService } from 'angular2-token';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'lodash';


@Injectable()
export class WorkExperienceService {

  constructor(private http: Angular2TokenService,
              private _notification: NotificationsService) { }

  fetchData(handle = '') {
    return this.http.get('experiences?handle=' + handle ).map(res => res.json());
  }

  saveData(data) {
    const self = this;
    const errors = [];
    _.each(data, (value, key) => {
      if (!value && value !== false) {
        errors.push(_.startCase(key.replace(/_/g, ' ')));
      }
    });
    if (errors.length) {
      self._notification.warn('Validation Failed', errors.join(', ') + ' can\'t be blank.');
    }
    if (data.id) {
      const id = data.id
      return this.http.put('experiences/' + id, { experience: data }).map(res => res.json());
    } else {
      return this.http.post('experiences', { experience: data }).map(res => res.json());
    }
  }

  deleteData(id) {
    return this.http.delete('experiences/' + id, {}).map(res => res.json());
  }

}
