import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';

import { UserService } from '../user.service';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-public-profile',
  templateUrl: './public-profile.component.html',
  styleUrls: ['./public-profile.component.css']
})
export class PublicProfileComponent implements OnInit {

  user = {};
  profile = {
    overview: null,
    title: null,
    birthday: null,
    address: null,
    profile_photos: []
  };
  handle = null;

  constructor(private _router: Router,
              private _active_router: ActivatedRoute,
              private _notification: NotificationsService,
              private _userService: UserService,
              private _profileService: ProfileService) { }

  ngOnInit() {
    const self = this;
    self._active_router.params.subscribe((params) => {
      const handle = params['handle'] ? params['handle'] : '';
      self._userService.fetchData(handle).subscribe(
        (res) => {
          if (res) {
            self.user = res;
          } else {
            self._notification.warn('Not Found', 'Profile handle not found.');
            self._router.navigateByUrl('/profile');
            return;
          }
        },
        (error) => { self._notification.error('Request Failed', error); }
      );

      self._profileService.fetchData(handle).subscribe(
        (res) => {
          if (res) {
            self.profile = res;
          }
        },
        (error) => { self._notification.error('Request Failed', error); }
      );
    });
  }

}
