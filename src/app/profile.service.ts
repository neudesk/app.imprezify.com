import { Injectable } from '@angular/core';
import { Angular2TokenService } from 'angular2-token';

@Injectable()
export class ProfileService {

  constructor(private http: Angular2TokenService) { }

  fetchData(handle = '') {
    return this.http.get('profiles?handle=' + handle ).map(res => res.json());
  }

  saveData(data) {
    if (data.id) {
      const id = data.id
      return this.http.put('profiles/' + id, { profile: data }).map(res => res.json());
    } else {
      return this.http.post('profiles', { profile: data }).map(res => res.json());
    }
  }

}
