import { Component, OnInit } from '@angular/core';
import { Angular2TokenService } from 'angular2-token';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  signupData = {
    email:                null,
    password:             null,
    passwordConfirmation: null,
    name: null
  };

  signinData = {
    email:                null,
    password:             null,
  };

  currentTab = 'register';

  constructor(private _tokenService: Angular2TokenService,
              private _notification: NotificationsService,
              private _router: Router) { }

  ngOnInit() {
  }

  signup() {
    const self = this;
    const errors = [];

    _.each(self.signupData, (value, key) => {
      if (value == null) {
        errors.push(_.startCase(key.replace(/_/g, ' ')) + ' can\'t be blank.');
      }
    });

    if (errors.length) {
      self._notification.warn('Validation Failed', errors.join('\n\n'));
      return;
    }

    self._tokenService.registerAccount(self.signupData).subscribe(
      res => {
        self._notification.success('Success', 'We have created your account. please login.');
        self.currentTab = 'login';
      },
      error => {
        self._notification.error('Error', JSON.parse(error._body).errors.full_messages[0]);
      }
    );
  }

  signin() {
    const self = this;

    if (self.signinData.email == null || self.signinData.password == null) {
      self._notification.error('Validation Failed', 'Please fill up all fields.');
      return;
    };

    self._tokenService.signIn(self.signinData).subscribe(
      res => {
        self._router.navigateByUrl('/edit/profile');
        console.log('res', res);
      },
      error => {
        self._notification.error('Error', JSON.parse(error._body).errors.full_messages[0]);
      }
    );
  }

}
