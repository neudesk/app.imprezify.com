import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { HomepageComponent } from './homepage/homepage.component';
import { PublicProfileComponent } from './public-profile/public-profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';

export const router: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'edit/profile', component: EditProfileComponent, canActivate: [Angular2TokenService] },
  // { path: 'profile/:handle', component: PublicProfileComponent, canActivate: [Angular2TokenService] },
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
